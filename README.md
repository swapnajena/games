## Games
## About this project:
* Android application that shows Games information using the Unibet games REST API.
* Games app demonstrates utilizing Android Jetpack components to create a simple Games list app.

### Architectural Patterns
Model-View-ViewModel, Dependency Injection.

## Android Features used:
* Architecture Pattern : MVVM
* Language : Kotlin
* Service call: Retrofit
* DataBinding
* RxJava

## Libraries and Concepts used:
* [Foundation] - Components for core system capabilities, Kotlin extensions and support for
  multidex and automated testing.
  * [AppCompat] - Degrade gracefully on older versions of Android.
  * [Android KTX] - Write more concise, idiomatic Kotlin code.
* [Architecture] - A collection of libraries that help design robust, testable, and
  maintainable apps. 
  * [Data Binding]- Declaratively bind observable data to UI elements.
  * [Lifecycles] - Create a UI that automatically responds to lifecycle events.
  * [LiveData] - Build data objects that notify views when the underlying database changes.
  * [ViewModel] - Store UI-related data that isn't destroyed on app rotations. Easily schedule
     asynchronous tasks for optimal execution.
* [UI] - Details on why and how to use UI Components in your apps - together or separate

  * [Layout] - Lay out widgets using different algorithms.
  
* Third party
  * Dagger2 for Dependency Injection
  * [Glide] for image loading
  
## Future Enhancements:
  * Offline data storage and caching : It would help to display existing data incase network failure.
  * Including Instrumentation Unit test.
  * UI enhancement.



