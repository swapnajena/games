package com.swapna.games.data.model

import com.google.gson.annotations.SerializedName

/**
 * Data model class to provide games detail info
 */

data class GamesInfo (@SerializedName("gameName") val gameName: String,
                      @SerializedName("imageUrl") val imageUrl: String
                      )
