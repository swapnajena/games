package com.swapna.games.data.model

import com.google.gson.annotations.SerializedName
/**
Data model class which will handle dynamic games tag by using Map
 */

data class Games(@SerializedName("games") val gamesInfo: Map<String, GamesInfo>) {

}