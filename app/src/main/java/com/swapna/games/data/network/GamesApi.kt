package com.swapna.games.data.network

import com.swapna.games.data.model.Games
import com.swapna.games.data.model.GamesInfo
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface to construct api query params
 */

interface GamesApi {
    @GET("getGamesByMarketAndDevice.json")
    fun getGamesInfo(
        @Query("jurisdiction") jurisdiction: String,
        @Query("brand") brand: String,
        @Query("deviceGroup") deviceGroup: String,
        @Query("locale") locale: String,
        @Query("currency") currency: String,
        @Query("categories") categories: String,
        @Query("clientId") clientId: String
    ): Single<Games>
}