package com.swapna.games.data.network

import com.swapna.games.data.model.Games
import com.swapna.games.data.model.GamesInfo
import com.swapna.games.di.DaggerApiComponent
import io.reactivex.Single
import javax.inject.Inject

class GamesApiService {
    @Inject
    lateinit var api: GamesApi

    init {
        DaggerApiComponent.create().inject(this)
    }

    fun getGames(jurisdiction: String, brand: String, deviceGroup: String, locale: String, currency: String,
                 categories: String, clientId: String ): Single<Games> {
        return api.getGamesInfo(jurisdiction, brand, deviceGroup, locale,
            currency, categories, clientId )
    }
}