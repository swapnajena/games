package com.swapna.games.di

import com.swapna.games.ui.GamesViewModel
import dagger.Component
import javax.inject.Singleton

/**
 *GamesViewModelComponent uses
 @Singleton Scope annotation will have a unique instance in this Component
 * This component adds info from the ApiModule and lists all classes that can be injected by this component
 */

@Singleton
@Component(modules = [ApiModule::class])
interface GamesViewModelComponent {
    fun inject(viewModel: GamesViewModel)
}