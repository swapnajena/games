package com.swapna.games.di

import com.swapna.games.data.network.GamesApiService
import dagger.Component

//Definition of a dagger component
@Component(modules = [ApiModule::class])
interface ApiComponent {

    /**
     * classes that can be injected by this component
     */
    fun inject(service: GamesApiService)

}