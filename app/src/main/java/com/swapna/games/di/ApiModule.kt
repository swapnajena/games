package com.swapna.games.di

import com.swapna.games.data.network.GamesApi
import com.swapna.games.data.network.GamesApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Tells dagger how to provide dependencies from the dependency graph
 */
//Retrofit api call to fetch list of games
@Module
open class ApiModule {
    private val BASE_URL = "https://api.unibet.com/game-library-rest-api/"

    @Provides
    fun provideGamesApi(): GamesApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(GamesApi::class.java)
    }

    /**
     *  Makes Dagger provide GamesAPiService
     */


    @Provides
    open fun provideGamesApiService(): GamesApiService {
        return GamesApiService()
    }
}