package com.swapna.games.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.swapna.games.R
import com.swapna.games.data.model.GamesInfo
import kotlinx.android.synthetic.main.activity_games.*


class GamesActivity : AppCompatActivity() {
    private lateinit var viewModel: GamesViewModel
    private val listAdapter = GamesAdapter(arrayListOf())

    /**
     * LiveData observer that observes list of games data coming from api call and populate data using adapter
     */

    private val gamesDataObserver = Observer<List<GamesInfo>> { list ->
        list?.let {
            gamesList.visibility = View.VISIBLE
            listAdapter.updateGamesList(it)
        }
    }

    /**
     * LiveData observer that observes a boolean value to indicate the loading state
     */

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        loadingView.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            listError.visibility = View.GONE
            gamesList.visibility = View.GONE
        }
    }

    /**
     * LiveData observer that observes the error state in api call and handles visibility of views
     */

    private val errorLiveDataObserver = Observer<Boolean> { isError ->
        listError.visibility = if (isError) View.VISIBLE else View.GONE
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_games)

        observeData()
        setUpUI()
    }
    private fun observeData(){
        viewModel = ViewModelProviders.of(this).get(GamesViewModel::class.java)
        viewModel.allGamesInfoList.observe(this, gamesDataObserver)
        viewModel.loading.observe(this, loadingLiveDataObserver)
        viewModel.loadError.observe(this, errorLiveDataObserver)

        loadData()

    }

    private fun setUpUI(){
        gamesList.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = listAdapter
        }

        /**
         * Refresh Layout refresh listener that will refresh the contents of the list by a fresh api call
         */

        refreshLayout.setOnRefreshListener {
            gamesList.visibility = View.GONE
            listError.visibility = View.GONE
            loadingView.visibility = View.VISIBLE
            loadData()
            refreshLayout.isRefreshing = false
        }
    }

    private fun loadData (){
        /**
         * Currently I have used hard coded value for api call as there is no UI interactions to fetch any dynamic value.
         * In future I will make it dynamic as per the business logic and and will handle this from UI.
         */

        viewModel.refresh("UK", "unibet", "mobilephone", "en_GB", "GBP",
            "casino,softgames", "casinoapp")
    }
}