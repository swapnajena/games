package com.swapna.games.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.swapna.games.data.model.Games
import com.swapna.games.data.model.GamesInfo
import com.swapna.games.data.network.GamesApiService
import com.swapna.games.di.DaggerGamesViewModelComponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * ViewModel for GamesActivity screen.
 * GamesViewModel fetches data from server and stores in LiveData for further use and handles all business logic
 */


class GamesViewModel : ViewModel {
    constructor() {

    }

    //constructor to check the callback from test to handle the dagger Component builder
    constructor(test: Boolean = true) {
        injected = true
    }

    val allGamesInfoList by lazy { MutableLiveData<List<GamesInfo>>() }
    val loadError by lazy { MutableLiveData<Boolean>() }
    val loading by lazy { MutableLiveData<Boolean>() }

    private val disposable = CompositeDisposable()

    @Inject
    lateinit var apiService: GamesApiService
    private var injected = false


    fun inject() {
        if (!injected) {
            DaggerGamesViewModelComponent.builder()
                    .build()
                    .inject(this)

        }
    }


    fun refresh(jurisdiction: String, brand: String, deviceGroup: String, locale: String, currency: String,
                categories: String, clientId: String ) {
        inject()
        loading.value = true
        getGames(jurisdiction, brand, deviceGroup, locale, currency,
                categories, clientId)

    }

    /**
     * Method that makes Retrofit api call and fetches result
     *Now it always fetches data from remote. I would like to implement offline storage and caching in future
     */
    private fun getGames(jurisdiction: String, brand: String, deviceGroup: String, locale: String, currency: String,
                         categories: String, clientId: String ) {
        disposable.add(
                apiService.getGames(jurisdiction, brand, deviceGroup, locale, currency,
                        categories, clientId)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<Games>() {
                            override fun onSuccess(gamesObj: Games) {
                                loadError.value = false
                                allGamesInfoList.value = gamesObj.gamesInfo.values.toList()
                                loading.value = false
                            }

                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                e.printStackTrace()
                                loading.value = false
                                allGamesInfoList.value = null
                                loadError.value = true
                            }

                        })
        )
    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}