package com.swapna.games.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.swapna.games.R
import com.swapna.games.data.model.Games
import com.swapna.games.data.model.GamesInfo
import com.swapna.games.databinding.GamesItemBinding

/**
 * Adapter class that binds data and images to recyclerview
 */

class GamesAdapter (private val gamesList: ArrayList<GamesInfo>) :
    RecyclerView.Adapter<GamesAdapter.GamesViewHolder>() {

    fun updateGamesList(newGamesList: List<GamesInfo>) {
        gamesList.clear()
        gamesList.addAll(newGamesList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<GamesItemBinding>(
            inflater,
            R.layout.games_item,
            parent,
            false
        )
        return GamesViewHolder(view)
    }

    override fun getItemCount() = gamesList.size

    override fun onBindViewHolder(holder: GamesViewHolder, position: Int) {
        holder.view.games = gamesList[position]
    }


    class GamesViewHolder(var view: GamesItemBinding) : RecyclerView.ViewHolder(view.root)
}
