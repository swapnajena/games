package com.swapna.games

import com.swapna.games.data.network.GamesApiService
import com.swapna.games.di.ApiModule
/**
Fake ApiModule class for mocking the api call
 */

class ApiModuleTest(private val mockService: GamesApiService) : ApiModule() {
    override fun provideGamesApiService(): GamesApiService {
        return mockService

    }
}
