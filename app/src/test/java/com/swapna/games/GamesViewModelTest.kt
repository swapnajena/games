package com.swapna.games

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.swapna.games.data.model.Games
import com.swapna.games.data.model.GamesInfo
import com.swapna.games.data.network.GamesApiService
import com.swapna.games.di.DaggerGamesViewModelComponent
import com.swapna.games.ui.GamesViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor

class GamesViewModelTest {
    @get:Rule
    var rule = InstantTaskExecutorRule()

    /**
     * Mocking the GamesApiService as we can not use actual api service for testing
     */
    @Mock
    lateinit var gamesApiService: GamesApiService

    private var gamesViewModel = GamesViewModel(true)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        DaggerGamesViewModelComponent.builder()
                .apiModule(ApiModuleTest(gamesApiService))
                .build()
                .inject(gamesViewModel)
    }

    /**
     * Asserting if the expected data and actual data matches in case of api success
     */
    @Test
    fun getGamesSuccess() {

        val gamesObj = Games(mapOf("key1" to GamesInfo("gameName1", "gameUrl1"),
                "key2" to GamesInfo("gameName2", "gameurl2")))

        val testSingle = Single.just(gamesObj)

        Mockito.`when`(gamesApiService.getGames("UK", "unibet", "mobilephone", "en_GB", "GBP",
                "casino,softgames", "casinoapp")).thenReturn(testSingle)

        gamesViewModel.refresh("UK", "unibet", "mobilephone", "en_GB", "GBP",
                "casino,softgames", "casinoapp")

        Assert.assertEquals(2, gamesViewModel.allGamesInfoList.value?.size)
        Assert.assertEquals("gameName1", gamesViewModel.allGamesInfoList.value?.get(0)?.gameName)
        Assert.assertEquals(false, gamesViewModel.loadError.value)
        Assert.assertEquals(false, gamesViewModel.loading.value)
    }

    @Test
    fun getGamesFailure() {

        val testSingle = Single.error<Games>(Throwable())


        Mockito.`when`(gamesApiService.getGames("UK", "unibet", "mobilephone", "en_GB", "GBP",
                "casino,softgames", "casinoapp")).thenReturn(testSingle)


        gamesViewModel.refresh("UK", "unibet", "mobilephone", "en_GB", "GBP",
                "casino,softgames", "casinoapp")

        Assert.assertEquals(null, gamesViewModel.allGamesInfoList.value)
        Assert.assertEquals(false, gamesViewModel.loading.value)
        Assert.assertEquals(true, gamesViewModel.loadError.value)
    }


    @Before
    fun setupRxSchedulers() {
        val immediate = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)
            }
        }

        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
    }


}